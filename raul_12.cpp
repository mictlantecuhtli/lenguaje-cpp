/*
Fecha:
Autor: Fatima Azucena Martínez Cadena
Correo: fatimaazucenamartinez274@gmail.com
*/

/*Un vendedor recibe un sueldo base más un 10% por comision de sus ventas
el vendedor desea saber cuanto dinero obtendra por concepto de comisiones
por las 3 ventas que realiza en el mes y el total que recibira en el mes
tomando en cunta su base y comisiones*/

#include <iostream>
using namespace std;

int main(){//Inicio metodo principal
        //Declaracion de variables
        float comision = 0;
        int cambio = 0;
        float sueldoFijo;
        int numVenta;
        float precioVenta,
        float sueldoTotal;
        cout<<"Ingrese su sueldo base: ";
        cin>>sueldoFijo;
        cout<<"Ingrese el numero de ventas realizadas: ";
        cin>>numVenta;
        for(int i = 0; i <= numVenta; i++){//Inicio for_1
                cambio = cambio + 1;
                cout<<"Ingrese el precio de la venta: "<<cambio;
                cin>>precioVenta;
                comision = comision + (precioVenta*0.10);
        }//Fin for_
        sueldoTotal = comision + sueldoFijo;
        cout<<"El total de por concepto de comision es de: "<<comision<<" pesos";
        cout<<"Su sueldo total es de: "<<sueldoTotal<<" pesos";
}//Fin metodo principal


