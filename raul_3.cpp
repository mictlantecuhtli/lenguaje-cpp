/*
 *Fecha: 05_10_2020
 *Autor: Fatima Azucena Martinez Cadena. 
 *fatimaazucenamartinez274@gmail.com*/

/*Calcula la cantidad de euros a monedas*/

#include <iostream>
using namespace std;

int main(){/*Inicio metodo principal*/
        /*Declaracion de variables*/
        double EURO = 0.039783;
        double conver = 0;
        int opcion;
        int continuar;
        int pesoMexicano;
        double DOLLAR = 22.91;

        do{/*Inicio do-while*/
                cout<<"*Bienvenido al menú de conversiones*\n1)Dolares\n2)Euros";
                cout<<"\nDigite su opcion: ";
                cin>>opcion;

                switch (opcion){/*Inicio switch case*/
                        case 1:
                                cout<<"Usted a elegido una conversion de pesos mexicanos a dolares";
                                cout<<"\nIngrese la cantidad de dinero que desea convertir: ";
                                cin>>pesoMexicano;
                                conver = (pesoMexicano/DOLLAR);
                                cout<<"Usted tiene: $"<<conver<<" dolares";
                        break;
                        case 2:
                                cout<<"Usted a eligo la conversion de pesos mexicanos a euros";
                                cout<<"\nIngrese la cantidad de dinero que desea convertir: ";
                                cin>>pesoMexicano;
				conver = (pesoMexicano/EURO);
                                cout<<"Usted tiene: €"<<conver<<" euros";
                        break;
                        default:
                                cout<<"Opcion no valida";
                }
                cout<<"\n¿Que desea hacer?\n1)Volver\n0)Salir";
                cout<<"\nEliga su opcion: ";
                cin>>continuar;
        }while(continuar == 1);
                cout<<"Hasta la proxima";
}
