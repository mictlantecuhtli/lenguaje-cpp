/*
 *Fecha: 07_10_2020
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com*/

/*Calcular el descuento y el monto a pagar por un medicamento
cualquiera en una farmacia si todos los medicamentos tienen un 
descuento del 35%.*/

#include <iostream>
using namespace std;

int main(){/*Inicio metodo principal*/
        /*Declaracion de variables*/
        float DES = 0.35;
        string nombre;
        float precio;

        cout<<"Inserte el nombre del medicamento: ";
        cin>>nombre;
        cout<<"Inserte el precio del medicamento: ";
        cin>>precio;

        precio = precio-(precio*DES);

        cout<<"El total a pagar por su "<<nombre<<" es de: "<<precio<<" pesos";
}/*Fin metodo principal*/
