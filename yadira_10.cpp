/*
Fecha: 09_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*Calcule la sumatoria de N numeros*/

#include <iostream>
using namespace std;

int main(){//Inicio método principal
	//Declaracion de variables
	int numeros [100];
	int cantidad_Numeros;
	int sumatoria_Numeros;

	cout<<"¿Cuantos números quiere sumar?: ";
	cin>>cantidad_Numeros;

	for ( int i = 1; i <= cantidad_Numeros; i++){//Inicio for_1
		cout<<"Ingrese el numero "<<i<<": ";
		cin>>numeros[i];

		sumatoria_Numeros += numeros[i];
	}//Fin for_1
	cout<<"La sumatoria de los numeros es: "<<sumatoria_Numeros;
}//Fin método principal
