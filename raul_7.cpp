/*
 *Fecha: 17_11_20
 *Autor: Fatima Azucena MC
 fatimaazucenamartinez274@gmail.com*/

/*Escriba un algoritmo que dada la cantidad de monedas de 5-10-20 pesos, 
diga la cantidad de dinero que se tiene en total.*/

#include <iostream>
using namespace std;

int main(){//Inicio metodo principal
	//Declaracion de variables
	int convertidor=0;
        float resultado=0;
        float cajero[ ] = { 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1, .50, .20, .10 };


        for(int i=0;i<=12;i++){/*Inicio for 1*/
                cout<<"Ingrese la  cantidad de  $"<<cajero[i]<<" pesos que tiene : "<<"\n";
                cin>>convertidor;
                resultado=resultado+(convertidor*cajero[i]);
        }/*Fin for 1*/
        cout<<"El total que tiene es de: $"<<resultado<<" pesos";
}//Fin metodo principal
