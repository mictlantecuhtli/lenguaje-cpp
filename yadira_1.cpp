/*
Fecha: 06_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmial.com
*/

/**Genera un programa que determine si eres de mayor de edad*/

//Libreria principal
#include <iostream>
using namespace std;

int main(){//Inicio método principal
	//Declaracion devariables
	int edad;

	cout<<"Digite su edad: ";
	cin>>edad;

	if( edad >= 18){
		cout<<"Usted es mayor de edad :D";
	}
	else{
		cout<<"Usted no es mayor de edad :(";
	}
}//Fin método principal
