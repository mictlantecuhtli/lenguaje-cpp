/*
 *Fecha: 21-08-2020 
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com*/

/*Realice un algoritmo para leer las calificaciones de N alumnos y de­termine el número de aprobados y reprobados.*/

/*Libreria principal*/
#include <iostream>
using namespace std;

int main(){/*Inicio de metodo principal*/
	/*Declaracion de variables*/
	double calificacion;
	int numeroAlumnos;
	int i;

	cout<<"Ingrese la cantidad de alunmos: ";
	cin>>numeroAlumnos;

	for (i=1;i<=numeroAlumnos;i++){/*Inicio for 1*/
		cout<<"Ingrese la calificacion del alumno: ";
		cin>>calificacion;

		if (calificacion>=6.0){/*Inicio condicional if-else*/
			cout<<"Alumno aprobado :D "<<"\n";
		}
		 else{
		 	cout<<"Alumno no aprobado :("<<"\n";
		 }/*Fin condicional if-else*/
	}/*Fin for 1*/
}/*Fin metodo principal*/
