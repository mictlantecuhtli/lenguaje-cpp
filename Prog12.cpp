/*
 * Fecha:09-05-2020
 * Autor:Fatima Azucena MC
 * <fatimaazucenamartinez274@gmail.com*/
 #include <iostream>
 using namespace std;
 //El gerente de una compañía automotriz desea determinar el im­puesto 
 //que va a pagar por cada uno de los automóviles que posee, además del 
 //total que va a pagar por cada categoría y por todos los vehículos, 
 //basándose en la siguiente clasificación:
 //Los vehículos con clave 1 pagan 10% de su valor.
 //Los vehículos con clave 2 pagan 7% de su valor.
 //Los vehículos con clave 3 pagan 5% de su valor.
 //Realice un algoritmo para obtener la información y represéntelo 
 //mediante diagrama de flujo, pseudocódigo. Los da­tos son la clave y 
 //costo de cada uno.
int main(){//Inicio metodo principal
	float montoPagar=0;
	int tama;

	
	struct CARRO{
		float precioxCarro;
		int clave;
	};
	cout<<"Ingrese el tamaño del arreglo:";
	cin>>tama;
	struct CARRO arreglo[tama];
	
	//Inicio for 1
	for(int i=0;i<=tama;i++){
	   cout<<"Ingrese el precio por carro: ";
	   cin>>arreglo[i].precioxCarro;
	   
	   cout<<"Ingrese la clave del carro:";
	   cin>>arreglo[i].clave;
    }//Fin for 1
	for(int i=0;i<=tama;i++){
		
		//Inicio if-else 
		//cout<<"Por aqui pasa"<<"\n";
		//cout<<arreglo[i].precioxCarro<<"\n";
		//if(arreglo[i].clave==1){
		   //montoPagar=montoPagar+(arreglo[i].precioxCarro*0.10);	
		//}else if(arreglo[i].clave==2){
			//montoPagar=montoPagar+(arreglo[i].precioxCarro*0.07);
		 //}else if(arreglo[i].clave==3){
			//montoPagar=montoPagar+(arreglo[i].precioxCarro*0.05);
		  //}//fin else
		  
		  //Inicio switch 
		  switch(arreglo[i].clave){
			  case 1:
			     montoPagar=montoPagar+(arreglo[i].precioxCarro*0.10);
			  break;
			  case 2:
			     montoPagar=montoPagar+(arreglo[i].precioxCarro*0.07);
			  break;
			  case 3:
			     montoPagar=montoPagar+(arreglo[i].precioxCarro*0.05);
			  break;
			  default: cout<<"Opcion no valida";
	      }//Fin switch
	}//Fin for 2
	cout<<"El total a pagar es: "<<montoPagar<<" pesos";
		
}//Fin metodo principal
