/*
Fecha: 07_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*Genera un programa que determine si la calificación es aprobatoria o no 
*/

#include <iostream>
using namespace std;

int main(){//Inicio método principal
	//Declaración de variables
	float calificacion;

	cout<<"	Ingrese su calificacion: ";
	cin>>calificacion;

	if ( calificacion >= 6.0){
		cout<<"El alumno tiene una calificacion aprobatoria";
	}
	else {
		cout<<"El alumno tiene una calificacion no aprobatoria";
	}
}//Fin método principal
