/*
Fecha: 13_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/* EN EL TECNOLÓGICO NACIONAL DE MEXICO SE PUBLICÓ UNA
CONVOCATORIA DE ESTUDIOS EN EL EXTRANJERO, PODRÁN PARTICIPAR AQUELLOS
ESTUDIANTES QUE TENGAN UN PROMEDIO MAYOR A 90. LOS ALUMNOS QUE HAYAN
ALCANZADO LA CALIFICACIÓN ENTRARÁN DIRECTO A LA TÓMBOLA PARA EL SORTEO.
LOS ALUMNOS QUE NO ALCANCEN EL PROMEDIO PODRÁN PARTICIPAR EN LA SIGUIENTE
CONVOCATORIA SI APRUEBAN EL EXAMEN DEL IDIOMA PARA EL SIGUIENTE SEMESTRE
Y MEJORAN EN UN 20% SU PROMEDIO
*/

#include <iostream>
using namespace std;

int main(){//Inicio método principal
	//Declaracion de variables
	int calificacion;
        int opcion;
        int opcion_Reintento;
        int examen_Idiomas;
        int continuar;
	int calificacion_Anterior;
		
	do{//Inicio do-while
			calificacion = 0;
			cout<<"B I E N V E N I D O S  A  L A  C O N V O C A T O R I A ";
			cout<<"\n\t¿Vas a registrarte por primera vez?:\n\t1)Si\n\t2)No ";
			cout<<"\n\tDigite su opcion: ";
			cin>>opcion;

			switch (opcion){//Inicio switch-case
				case 1:
					cout<<"\n\tDigita tu calificacion (0-100): ";
					cin>>calificacion;
					
						if( calificacion >= 90){
							cout<<"\n\tHAZ ENTRADO DIRECTO A LA TOMBOLA";		
						}
						else{
							cout<<"\n\tNO PUEDES ENTRAR A LA TOMBOLA, SUERTE PARA LA PROXIMA";
						}
				break;
				case 2:
					cout<<"\n\tTu ya haz participado anteriormermente, el requisito ahora para que puedas ent                                        rar es: pasar el examen de idiomas y mejorar un 20 porciento en tu calificacion anterior";
					cout<<"\n\t¿Pasaste el examen de idiomas?:\n\t1)Si\n\t2)No";					     
					cout<<"\n\tDigite su opcion: ";
					cin>>examen_Idiomas;

						if ( examen_Idiomas == 1){
							cout<<"\n\tDigite su calificacion anterior: ";
							cin>>calificacion_Anterior;
							cout<<"\n\tDigite su calificacion actual: ";
							cin>>calificacion;
								if ( calificacion > 90){
									cout<<"\n\tFELICIDADES , ENTRASTE A LA TOMBOLA, CUMPLES"; 
									cout<<"\n\tCON LOS 2 REQUISITOS";
								}
								else{
									cout<<"\n\tNO PUEDES ENTRAR A LA TOMBOLA, SUERTE PARA LA                                                                         PROXIMA";
								}
						}
						else if( examen_Idiomas == 2){
							cout<<"\n\tNO PUEDES ENTRAR A LA TOMBOLA, YA QUE NO PASASTE UNO DE LOS REQUISITOS, SUERTE PARA                                                        LA PROXIMA";
						}
					
				break;
				default:
					cout<<"\n\tOpcion no válida, intente de nuevo";
				break;
			}//Fin switch-case
			cout<<"\n\t¿Que desea hacer?:\n\t1)Volver al menú\n\t0)Salir";
			cout<<"\n\tDigite su opcion: ";
			cin>>continuar;
		}while( continuar == 1);
		cout<<"\n\tHasta la proxima";
}//Fin método principal
