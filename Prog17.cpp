/*
 * Fecha:12-08-2020
 * Autor:Fatima Azucena MC
 * fatimaazucenamartinez274@gmail.com*/

/*La compañía de autobuses “La curva loca” requiere determinar el costo que tendrá el boleto de un viaje sencillo, esto basado en los kilómetros por recorrer y en el costo por kilómetro. Realice un diagrama de flujo y pseudocódigo que representen el algoritmo para tal fin.
*/

/*Libreria principal*/
#include <iostream>
using namespace std;

int main(){/*Inicio merodo principal*/
        /*Declaracion de variables*/
      	float costoBoleto=0;
        float precioKilometro=0;
        int kilometrosRecorridos;
        
	/*Datos ingresados por usuario*/
        cout<<"¿Cual es el costo del kilometro?: ";
        cin>>precioKilometro;
        cout<<"¿Cuantos kilmetros va a recorrer?: ";
        cin>>kilometrosRecorridos;

        costoBoleto=(precioKilometro*kilometrosRecorridos);
        cout<<"El precio del boleto es de: "<<costoBoleto<<" pesos";
}/*Fin de metodo principal*/

