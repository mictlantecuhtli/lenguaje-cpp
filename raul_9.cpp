/*
Alumna: Fatima Azucena MC
Fecha: 24_11_2020
Correo: fatimaazucenamartinez274@gmail.com
*/

/*Calcular el nuevo salario de un empleado si se le descuenta el 20% de su salario actual.*/

//Libreria principal
#include <stdio.h>
using namespace std;

int main(){//Inicio metodo principal
	//Declaracion de variables
	String nombre;
	double DESCUENTO = 0.20;
	double salario, nuevo_Salario;

	cout<<"Ingrese el nombre del empleado: ";
	cin>>nombre;
	cout<<"Ingrese el salario actual: ";
	cin>>salario;

	nuevo_Salario = nuevo_Salario-(salario*DESCUENTO);

	cout<<"El sueldo nuevo de "<<nombre<<" es de: "<<nuevo_Salario<<" pesos";
}//Fin metodo principal
