/*
Fecha: 08_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail*/

/*Genere el factorial de un número*/

#include <iostream>
using namespace std;

int main(){//Inicio método principal
	//Declaracion de variables
	int numero;
	int numero_Factorial = 1;

	cout<<"Ingrese un numero: ";
	cin>>numero;

	for ( int i = 1; i <= numero; i++){
		numero_Factorial = numero_Factorial * i;
	}
	cout<<"El factorial de "<<numero<<" es "<<numero_Factorial;

}//Fin método principal
