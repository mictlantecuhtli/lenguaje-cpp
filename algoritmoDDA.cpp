/*
Fecha: 16_06_2021
Autor: Fátima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

#include <stdio.h>
//#include <math.h>
#include <stdlib.h>
#include <iostream>
using namespace std;

int algoritmoDDA(int x1,int y1, int x2, int y2);

int main(){
	int a1;
	int b1;
	int a2;
	int b2;

	cout<<"Ingrese el valor de la coordenada x1: ";
	cin>>a1;
	cout<<"Ingrese el valor de la coordenada y1: ";
        cin>>b1;
	cout<<"Ingrese el valor de la coordenada x2: ";
        cin>>a2;
	cout<<"Ingrese el valor de la coordenada y2: ";
        cin>>b2;

	algoritmoDDA(a1,b1,a2,b2);	
}
int algoritmoDDA(int x1, int y1, int x2, int y2){
	int dx;
        int dy;
        int steps;
        int xinc;
        int yinc;

        dx = abs(x2-x1);
        dy = abs(y2-y1);

        if ( dx > dy){
                steps = dx;
        }
        else {
                steps = dy;
        }

        xinc = dx / steps;
        yinc = dy / steps;

	for (int i = 0; i <= steps; i++){
		cout<<"x: "<<x1<<" y: "<<y1<<"\n";
		x1 = x1 + xinc;
		y1 = y1 + yinc;
		
	}
	return 0;
}
