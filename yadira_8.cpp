/*
Fecha: 11_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com*/

/*DESCRIPCIÓN : CALCULAR EL PROMEDIO DE LAS EDADES DEL GRUPO DE PRIMER SEMESTRE*/

#include <iostream>
using namespace std;

int main(){//Inicio método principal
	//Declaracion de variables
	int numero_Alumnos;
	int  suma_Edades = 0;
	int edad [100];

	cout<<"¿Cunatos alumnos son?: ";
	cin>>numero_Alumnos;

	for ( int i = 1; i <= numero_Alumnos; i++){//Inicio for_1
		cout<<"Digite la edad del alumno "<<i<<": ";
		cin>>edad[i];
		suma_Edades += edad[i];
	}//Fin for_1
	suma_Edades = suma_Edades/numero_Alumnos;
	cout<<"El promedio de las edades es: "<<suma_Edades<<"\n";
}//Fin método principal
