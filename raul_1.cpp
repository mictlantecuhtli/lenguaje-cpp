/*Fecha: 06_10_2020
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com*/

/*Una persona recibe un préstamo de $10,000.00 de un
banco y desea saber cuánto pagará de interés, si el 
banco le cobfgra una tasa del 27% anual.*/

/*Libreria principal*/
#include <iostream>
using namespace std;

int main(){/*Inicio metodo principal*/
        /*Declaracion de variables*/
        int tiempo;
        float PRESTAMO=10000;
        int transcurrido;
        float INTERES=0.27;
        int i;

        cout<<"Inserte el año en que solicito el prestamo: ";
        cin>>tiempo;
        cout<<"Inserte el año actual: ";
        cin>>transcurrido;

        for(i=tiempo;i<=transcurrido;i++){/*Inicio for_1*/
                PRESTAMO=PRESTAMO+(PRESTAMO*INTERES);

                        cout<<"El interes del año "<<i<<" es de: $"<<PRESTAMO<<" pesos"<<"\n";
        }/*Fin for_1*/
}/*Fin metodo principal*/
