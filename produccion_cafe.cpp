/*
Fecha: 11_06_2021
Autor: Fátima Azucena MC
Correo: fatimaazucenamartinez274@gmial.com
*/

#include <iostream>
using namespace std;

int main(){//Inicio metodo principal
	//Declaracion de variables
	typedef char String [24];
	String nombres [12] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	int i;
	int posicion;
	double total_produccion;
	double suma_produccion [100];

	cout<<"Digite la producción de cáfe en KG por meses: \n";
	for (i = 0; i < 12; i++){//Inicio for_1
		cout<<"-->"<<nombres[i]<<": \n";
		cin>>suma_produccion[i];
		total_produccion = total_produccion + suma_produccion[i];
	}//Fin for_1

	total_produccion = total_produccion/12;
	cout<<"Producción anual: "<<total_produccion;

	cout<<\n"Mayor produccion que el promedio: ";
	 for (int i = 0; i < 12; i++){//Inicion for_1
                if (suma_produccion[i] > total_produccion){//Inicio if_1
                        cout<<"-->"<<suma_produccion[i]<<"\n";
                }
        }//Fin for_1

	 cout<<"Producción menor al promedio: ";
	 for (int i = 0; i < 12; i++){//Inicion for_1
                if (suma_produccion[i] < total_produccion){//Inicio if_1
                        cout<<"-->"<<suma_produccion[i]<<"\n";
                }
        }//Fin for_1

	 for (int x = 0; x < 12; x++){//Inicio for_2
            for (int y = 0; y < 11; y++){//Inicio for_2
                if (suma_produccion[y] > suma_produccion[y+1]){//Inicio condicional
		    posicion = suma_produccion[y + 1];
                    suma_produccion[y+1] = suma_produccion[y];
                    suma_produccion[y] = posicion;
                }//Fin condicional if
            }//Fin for_3
        }//Fin for_1

	 cout<<"La mayor produccion de cáfe es de: "<<suma_produccion[11]<<" kg";
         cout<<"\nLa menor produccion de cáfe es de: "<<suma_produccion[0]<<" kg \n";
}//Fin metodo priincipal
