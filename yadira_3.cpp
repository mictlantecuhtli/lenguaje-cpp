/*
Fecha: 06_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmial.com
*/

/* UN PROGRAMA SOLICITA QUE SEAN CAPTURADOS TRES DATOS
NUMÉRICOS Y A PARTIR DE ELLOS IMPRIMIRÁ SI EL NÚMERO ES PAR.*/

//Libreria principal
#include <iostream>
using namespace std;

int main(){//Inicio metodo principal
        //Declaracion de variables
        int numero_1;
        int numero_2;
        int numero_3;

        cout<<"Digite el primer número: ";
        cin>>numero_1;
        cout<<"Digite el segundo número: ";
        cin>>numero_2;
        cout<<"Digite el tercer número: ";
        cin>>numero_3;

        if ( numero_1%2==0){
                cout<<"El número "<<numero_1<<" es par";
        }
        else {
                cout<<"El numero "<<numero_1<<" es impar";
        }

        if ( numero_2%2==0){
                cout<<"El número "<<numero_2<<" es par";
        }
        else {
                cout<<"El numero "<<numero_2<<" es impar";
        }

        if ( numero_3%2==0){
                cout<<"El número "<<numero_3<<" es par";
        }
        else {
                cout<<"El numero "<<numero_3<<" es impar";
        }
}//Fin metodo principal

