/*
Fecha: 18_06_2021
Autor: Fátima Azucena MC
Correo: fatimaazucenamartínez274@gmail.com
*/

#include <iostream>
using namespace std;

int algoritmoBresenham(int x1, int y1, int x2, int y2);

int main(){//Inicio metodo principal
	//Declaracion de variables
	int a1;
	int b1;
	int a2;
	int b2;

	cout<<"Digite la coordenada de x1: ";
	cin>>a1;
	cout<<"Digite la coordenada de y1: ";
	cin>>b1;
	cout<<"Digite la coordenada de x2: ";
        cin>>a2;
        cout<<"Digite la coordenada de y2: ";
        cin>>b2;

	algoritmoBresenham(a1,b1,a2,b2);
}//Fin metodo principal
int algoritmoBresenham(int x1, int y1, int x2, int y2){
        int x, y, dx, dy, p;
        x = x1;
        y = y1;
        dx = x2 - x1;
        dy = y2 - y1;
        p = 2*dy - dx;

        while (x <= x2){
                cout<<"x: "<<x<<" y: "<<y<<"\n";
                x++;
                if ( p < 0){
                        p = p + 2 * dy;
                }
                else{
                        p = p + ( 2 * dy) - (2 * dx);
			y++;
                }
        }
	return 0;
}
