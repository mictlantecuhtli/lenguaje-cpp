/*
*Fecha: 26_10_20
*Autor: Fatima Azucena MC
*fatimaazucenamartinez274@gmail.com
*/

/*5. Calcular el nuevo salario de un empleado si obtuvo
un incremento del 8% sobre su salario actual y un descuento 
de 2.5% por servicios.*/

/*Libreria principal*/
#include <iostream>
using namespace std;
int main(){//Inicio metodo principal
	//Declaracion de variables
	float DESCUENTO = 0.08;
	float INCREMENTO = 0.025;
        string nombre;
	double salarioActual = 0;
        
        cout<<"Ingrese el nombre del empleado: ";
	cin>>nombre;
	cout<<"Ingrese su salario actual: ";
	cin>>salarioActual;

	salarioActual = salarioActual-(salarioActual*DESCUENTO);
	salarioActual = salarioActual-(salarioActual*INCREMENTO);

	cout<<"El salario actual de "<<nombre<<" es de: $"<<salarioActual<<" pesos\n";
}//Fin del metodo principal

