/*
Fecha: 07_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com*/

/*Elabora un programa con condicionales anidados que solicite 3 calificaciones, 
obtén el promedio de esas tres calificaciones, y de acuerdo al promedio que 
se obtuvo, coloca la leyenda que le corresponde, que encontraras en la imagen 
que te comparto con el nombre NIVEL DE DESEMPEÑO-TESJI.JPG.  
Por ejemplo si tu promedio se encuentra entre 95 y 100 deberá aparecer la 
leyenda "EXCELENTE" //El documento se encuentra adjunto*/

#include <iostream>
using namespace std;

int main(){//Inicio metodo principal
	//Declaracion de variables
	int calificacion_Fisica;
	int calificacion_Programacion;
	int calificacion_Historia;
	int promedio_General;

	cout<<"Digite la calificacion que obtuvo en Física: ";
	cin>>calificacion_Fisica;
	cout<<"Digite la calificacion que obtuvo en Programación: ";
	cin>>calificacion_Programacion;
	cout<<"Digite la calificacion que obtuvo en Historia: ";
	cin>>calificacion_Historia;

	promedio_General = (calificacion_Fisica + calificacion_Programacion + calificacion_Historia)/2;

	if ( ( promedio_General >=9.5) && ( promedio_General <= 10.0)){
		cout<<"EXCELENTE";
	}
		else if( ( promedio_General >= 8.5) && ( promedio_General <= 9.4)){
			cout<<"NOTABLE";
		}
			else if( (promedio_General >= 7.5) && ( promedio_General <= 8.4)){
				cout<<"BUENO";
			}
				else if( (promedio_General >= 7.0) && ( promedio_General <= 7.4)){
					cout<<"SUFICIENTE";
				}
					else if( promedio_General < 7.0){
						cout<<"DESEMPEÑO INSUFICIENTE";
					}

}//Fin metodo principal
