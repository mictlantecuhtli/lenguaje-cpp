/*
Fecha: 07_01_2020
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*Elabora un programa que compare 3 numeros
y los ordene de mayor a menor*/

#include <iostream>
using namespace std;

int main(){//Inicio metodo principal
	//Declaracion de variables
	int a;
	int b;
	int c;

	cout<<"Digite el primer numero: ";
	cin>>a;
	cout<<"Digite el segundo numero: ";
	cin>>b;
	cout<<"Digite el tercer numero: ";
	cin>>c;

	if ( a > b && b > c){//Inicio condicional if_else
		cout<<a;
		cout<<"\n"<<b;
		cout<<"\n"<<c;
	}
		else if( a > c && c > b){
			cout<<a;
			cout<<"\n"<<b;
			cout<<"\n"<<c;
		}
			else if ( b > a && a > c){
				cout<<b;
				cout<<"\n"<<a;
				cout<<"\n"<<c;
			}
				else if ( b > c && c > a){
					cout<<b;
					cout<<"\n"<<c;
					cout<<"\n"<<a;
				}
					else if ( c > a && a > b){
						cout<<c;
						cout<<"\n"<<a;
						cout<<"\n"<<b;
					}
						else if ( c > b && b > a){
							cout<<c;
							cout<<"\n"<<b;
							cout<<"\n"<<a;
						}//Fin if_else
}//Fin metodo principal
