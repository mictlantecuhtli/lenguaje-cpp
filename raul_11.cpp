/*
Fecha: 28_12_2020
Autor: Fatima Azucena MC
Correo: fatimaazucanmartinez274@gmail.com
*/

/*Obtener la edad de una persona en meses, si se ingresa su edad
en años y meses. Ejemplo: Ingresado 3 años 4 meses debe
mostrar 40 meses.*/

#include <iostream>
using namespace std;

int main(){//Inicio metodo principal
        //Declaracion de variables
        int meses;
        int anyos;
        int edad = 0;

        cout<<"Ingrese el numero de años que tiene: ";
        cin>>anyos;
        cout<<"Con cuantos meses: ";
        cin>>meses;

        edad = edad + (anyos*12) + meses;
	
        cout<<"Su edad en meses es de: "<<edad;
}//Fin metodo principal

