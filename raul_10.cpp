/*
Fecha: 26_12_2020
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*Concatenar 3 numeros o letras  mostrar el resultado*/

#include <iostream>
using namespace std;

int main(){//Inicio metodo principal
	//Declaracion de variables
	int valor_1;
	int valor_2;
        int valor_3;
	int valor_4;

	cout<<"Ingrese el primer numero o letra: ";
	cin>>valor_1;
	cout<<"Ingrese el segundo numero o letra: ";
	cin>>valor_2;
	cout<<"Ingrese el tercer numero o letra: ";
	cin>>valor_3;

	valor_4 = valor_1 + valor_2 + valor_3;

	cout<<"La concatenacion es: "<<valor_4;
}//Fin metodo principal
