/*
Fecha: 10_02_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail
*/

/*
Solicitar la calificacion de las materias
las suma y saca el promedio
*/

#include <iostream>
using namespace std;

int main(){//Inicio método principal
	//Declaracion de variables
        char name [20];
	string materias [3];
        int calificacion_materia [3][5];
        char apellido_Paterno [20];
        char apellido_Materno [20];
        int grupo;
        char carrera [20];
        double suma_Promedios = 0;
        double promedio_General;
        double suma_Unidades;
        int promedio_materia;
	
	cout<<"Digite su nombre: ";
	cin>>name;
	cout<<"Digite su apellido paterno: ";
	cin>>apellido_Paterno;
	cout<<"Digite su apellido materno: ";
	cin>>apellido_Materno;
	cout<<"Digite su grupo: ";
	cin>>grupo;
	cout<<"Digite su carrera: ";
	cin>>carrera;

	for ( int i = 0; i < 3; i++ ){//Inicio for_1
		cout<<"\nDigite el nombre de la materia "<<i + 1<<": ";
		cin>>materias[i];

		for (int j = 0; j < 5; j++){//Inicio for_2
			cout<<"Digite el promedio que obtuvo en la unidad "<<j + 1<<" (10-100): ";
			cin>>calificacion_materia[i][j];
		}//Fin for_2
	}//Fin for_1
	for ( int a = 0; a < 3; a++){//Inicio for_3
		promedio_materia = 0;
		suma_Unidades = 0;
		cout<<"\nAlumno: "<<name<<" "<<apellido_Paterno<<" "<<apellido_Materno;
		cout<<"\nGrupo: "<<grupo;
		cout<<"\nCarrera: "<<carrera;
		cout<<"\nMateria: "<<materias[a];
		cout<<"\n";
		cout<<"U1\tU2\tU3\tU4\tU5\n";

		for ( int b = 0; b < 5; b++){//Inicio for_4
			cout<<calificacion_materia[a][b]<<"\t";
			promedio_materia = promedio_materia + calificacion_materia[a][b];
		}//Fin for_4
		suma_Unidades = promedio_materia / 5;
		suma_Unidades = suma_Unidades + 1;
		cout<<"\n+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-++-";
		cout<<"\nPromedio: "<<suma_Unidades;
		cout<<"\n+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-++-";
	}//Fin for_3
	promedio_General = suma_Unidades / 3;
	cout<<"\nEl promedio general es : "<<promedio_General;
	cout<<"\n+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-++-\n";
}//Fin metodo principal
