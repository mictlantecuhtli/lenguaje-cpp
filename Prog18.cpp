/*Fecha: 17-08-2020
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com*/

/*Se desea saber el total de una caja registradora de un almacén, se conoce el número de billetes y monedas, así como su valor. Realice un algoritmo para determinar el total. Represente la solución me­diante el diagrama de flujo, el pseudocódigo.
*/

/*Librera principal*/
#include <iostream>
using namespace std;

int main(){/*Inicio metodo principal*/
        /*Declaracion de variables*/
        int convertidor=0;
        float resultado=0;
        float cajero[ ] = { 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1, .50, .20, .10 };


        for(int i=0;i<=12;i++){/*Inicio for 1*/
                cout<<"Ingrese la  cantidad de  $"<<cajero[i]<<" pesos que tiene : "<<"\n";
                cin>>convertidor;
                resultado=resultado+(convertidor*cajero[i]);
        }/*Fin for 1*/
        cout<<"El total de la caja registradora es de: $"<<resultado<<" pesos";
}/*Fin de metodo principal*/



