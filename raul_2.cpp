/*Fecha: 02_10_20
 *Autor: Fatima Azucena MC
 *fatimaazucenamartinez274@gmail.com*/

/*Calcula el precio de un boleto de viaje, tomando en cuenta 
el número de kilómetros que se van a recorrer. El precio por 
Kilometro es de $20.50*/

/*Libreria principal*/
#include <iostream>
using namespace std;

int main(){/*Inicio metodo principal*/
        /*Declaracion de variables*/
        double costoBoleto;
        int kilometrosRecorridos;
        double PRECIOKILOMETRO=20.5;

        cout<<"Ingrese los kilometros que va a recorrer: ";
        cin>>kilometrosRecorridos;

        costoBoleto=(kilometrosRecorridos*PRECIOKILOMETRO);

        cout<<"Usted a recorrido "<<kilometrosRecorridos<<" kilometros y el precio de su boleto es de: "<<costoBoleto<<" pesos"<<"\n";
}/*Fin metodo principal*/
