/*
 * Fecha:09-05-2020
 * Autor:Fatima Azucena MC
 * <fatimaazucenamartinez274@gmail.com*/
 #include <iostream>
 using namespace std;
 //Realice un algoritmo que determine el sueldo semanal de N 
 //traba­jadores considerando que se les descuenta 5% de su sueldo 
 //si ganan entre 0 y 150 pesos. Se les descuenta 7% si ganan más 
 //de 150 pero menos de 300, y 9% si ganan más de 300 pero menos de 
 //450. Los datos son horas trabajadas, sueldo por hora y nombre de 
 //cada tra­bajador. Represéntelo mediante diagrama de flujo, pseudocódigo.
 int main(){
   float sueldoHora;
   int numTrabajadores;
   float totalBruto=0;
   float totalNeto=0;
  
 	struct SUELDO{
		string nombre;
		int hTrabajadas;
		float dineroGanado;
		float sueldoBruto;
		float sueldoNeto;
	};
	
	cout<<"Ingrese el precio por hora: ";
	cin>>sueldoHora;
	cout<<"Ingrese el numero de trabajadores: ";
	cin>>numTrabajadores;
	struct SUELDO arreglo[numTrabajadores];
	
	//Inicio for 1
	cout<<"Pasa por aqui for 1"<<"\n";
	for(int i=0;i<numTrabajadores;i++){
		cout<<"Ingrese el nombre del trabajador: ";
		cin>>arreglo[i].nombre;
		cout<<"Ingrese las horas trabajadas: ";
		cin>>arreglo[i].hTrabajadas;
		cout<<"Ingrese el dinero ganado: ";
		cin>>arreglo[i].dineroGanado;
	}//Fin for 1
	
	cout<<"Pasa por aqui for 2:"<<"\n";
	//Inicio for 2
	for(int i=0;i<numTrabajadores;i++){
	
	    cout<<"Pasa por aqui else:"<<"\n";
		//Inicio if-else
		if((arreglo[i].dineroGanado>0)&&(arreglo[i].dineroGanado<=150)){
			arreglo[i].sueldoBruto=40*(arreglo[i].dineroGanado/arreglo[i].hTrabajadas);
			arreglo[i].sueldoNeto=arreglo[i].sueldoBruto-(arreglo[i].sueldoBruto*0.05);
		}else if ((arreglo[i].dineroGanado>150)&&(arreglo[i].dineroGanado<=350)){
			arreglo[i].sueldoBruto=40*(arreglo[i].dineroGanado/arreglo[i].hTrabajadas);
			arreglo[i].sueldoNeto=arreglo[i].sueldoBruto-(arreglo[i].sueldoBruto*0.07);
		 }else if((arreglo[i].dineroGanado>350)&&(arreglo[i].dineroGanado<=450)){
			arreglo[i].sueldoBruto=40*(arreglo[i].dineroGanado/arreglo[i].hTrabajadas);
			arreglo[i].sueldoNeto=arreglo[i].sueldoBruto-(arreglo[i].sueldoBruto*0.09);
		  }//Fin else
	}//Fin for 2
	
	cout<<"Pasa por aqui for 3"<<"\n";
	//Inicio for 3
	for(int i=0;i<numTrabajadores;i++){
		cout<<"El sueldo bruto de "<<arreglo[i].nombre<<" es "<<arreglo[i].sueldoBruto<<" pesos"<<"\n";
		cout<<"El sueldo neto de "<<arreglo[i].nombre<<" es "<<arreglo[i].sueldoNeto<<" pesos"<<"\n";
		totalBruto=totalBruto+arreglo[i].sueldoBruto;
		totalNeto=totalNeto+arreglo[i].sueldoNeto;
	}//Fin for 3
	cout<<"El suedo total bruto es: "<<totalBruto<<" pesos"<<"\n";
	cout<<"El sueldo total neto es: "<<totalNeto<<" pesos";
}//Fin main
