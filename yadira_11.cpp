/*
Fecha: 10_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com*/

/*Determine la tabla de multiplicar de un número*/

#include <iostream>
using namespace std;

int main(){//Inicio método principal
	int resultado;
	int numero;
	int tabla;

	cout<<"¿Que tabla desea ver?: ";
	cin>>tabla;
	cout<<"¿Hasta donde la desea ver?: ";
	cin>>numero;

	for(int i =1; i <= numero; i++){//Inicio método principal
		resultado = tabla * i;
		cout<<tabla<<" x "<<i<<" = "<<resultado;
		cout<<"\n";
	}//Fin método principal
}//Fin método principal
