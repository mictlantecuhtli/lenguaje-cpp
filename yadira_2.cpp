/*
Fecha: 08_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*EN UNA TIENDA DEPARTAMENTAL ESTA PUBLICANDO NUEVAS OFERTAS POR
TEMPORADA NAVIDEÑA. SI LOS CLIENTES PRESENTAN UNA TARJETA DE CLIENTE FRECUENTE.
LES OTORGARÁN UN DESCUENTO DEL 20% DEL TOTAL DE SU COMPRA.
*/
#include <iostream>
using namespace std;

int main(){//Inicio método principal
        //Declaracion de variables
        int opcion;
        int continuar;
        int numVentas;
        float precio_Venta = 0;
        float venta_Descuento [100];
        float venta_no_Descuento [100];

        do{
                cout<<"B I E N V E N I D O S\n\t¿Cuenta con tarjeta de cliente frecunete?:\n\t1)Si\n\t2)No";
                cout<<"\n\tDigite su respuesta: ";
                cin>>opcion;

                switch (opcion){
                        case 1:
                                cout<<"Usted cuenta con tarjeta de cliente frecuente";
                                cout<<"\nDigite el numero de ventas que realizo: ";
                                cin>>numVentas;

                                for ( int i = 1; i <= numVentas; i++){
                                        cout<<"Ingrese el precio de la venta "<<i<<" : $";
                                        cin>>venta_Descuento[i];
                                        precio_Venta += venta_Descuento[i];
                                }
                                precio_Venta = precio_Venta - (precio_Venta*0.20);

                                cout<<"El monto total es de: $"<<precio_Venta;
                                break;
                        case 2:
                                precio_Venta = 0;
                                cout<<"Usted no cuenta con tarjeta de cliente frecuente";
                                cout<<"\nDigite el numero de ventas que realizo: ";
                                cin>>numVentas;

                                for ( int i = 1; i <= numVentas; i++){
                                        cout<<"Ingrese el precio de la venta "<<i<<": $";
                                        cin>>venta_no_Descuento[i];
                                        precio_Venta += venta_no_Descuento[i];
                                }
                                cout<<"El monto total es : $"<<precio_Venta;
                        break;
                        default:
                                cout<<"Opcion no valida, intente de nuevo";
                        break;

                }
                cout<<"\n¿Que desea hacer?:\n1)Volver al ménu\n0)Salir";
                cout<<"\nDigite su opcion: ";
                cin>>continuar;

        }while (continuar == 1);
}//Fin metodo principal

