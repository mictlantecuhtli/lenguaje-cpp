/*
Fecha: 05_01_2021
Autor: Fatima Azucena MC
Correo: fatimaazucenamartinez274@gmail.com
*/

/*En un hospital existen 3 áreas: Urgencias, Pediatría y Traumatología. 
El presupuesto anual del hospital se reparte de la siguiente manera: 
Pediatría 42% y Traumatología 21%.*/

#include <iostream>
using namespace std;

int main(){//Inicio metodo principal
	//Declaracion de variables
	float presupuesto_Anual;
	float presupuesto_Traumatologia = 0;
	float presupuesto_Pedriatia = 0;
	float presupuesto_Urgencias = 0;
	float pediatria = 0.42;
	float traumatologia = 0.21;
	float urgencias = 0.37;
	
	cout<<"Ingrese el presupuesto anual: ";
	cin>>presupuesto_Anual;

	presupuesto_Traumatologia = presupuesto_Anual*traumatologia;
	presupuesto_Pediatria = presupuesto_Anual*pediatria;
	presupuesto_Urgencias = presupuesto_Anual*urgencias;

	cout<<"El presupuesto para el área de traumatología es de: $"<<presupuesto_Traumatología<<" pesos";
	cout<<"\nEl presupuesto para el área de pediatría es de: $"<<presupuesto_Traumatología<<" pesos";
	cout<<"\nEl presupuesto para el área de urgencias es de: $"<<presupuesto_Urgencias<<" pesos";
}//Inicio metodo principal
